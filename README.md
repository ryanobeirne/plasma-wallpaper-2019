# Plasma 5.16 Release: Wallpaper Competition

- [KDE Forum](https://forum.kde.org/viewforum.php?f=312)
- [My Submission](https://forum.kde.org/viewtopic.php?f=312&t=160786)

[logo]: bubbles_preview.png "Bubbles"

![alt text][logo]

[Additional Resolutions](content/images)